## aosp_RM6785-userdebug 12 SQ1D.220105.007 1642848233 test-keys
- Manufacturer: realme
- Platform: mt6785
- Codename: RM6785
- Brand: realme
- Flavor: aosp_RM6785-userdebug
- Release Version: 12
- Id: SQ1D.220105.007
- Incremental: 1642848233
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/raven/raven:12/SQ1D.220105.007/8030436:user/release-keys
- OTA version: 
- Branch: aosp_RM6785-userdebug-12-SQ1D.220105.007-1642848233-test-keys
- Repo: realme_rm6785_dump_12834


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
